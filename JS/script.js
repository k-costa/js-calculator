let currentInput = document.querySelector('.currentInput');
let answerScreen = document.querySelector('.answerScreen');
let buttons = document.querySelectorAll('button');
let eraseBtn = document.querySelector('#erase');
let clearBtn = document.querySelector('#c');
let evaluate = document.querySelector('#evaluate');
let screenValue = []

clearBtn.addEventListener("click", () =>{
    screenValue = [''];
    answerScreen.innerHTML = 0;
    currentInput.className = 'currentInput';
    answerScreen.className = 'answerScreen';
    answerScreen.style.color = "rgba(150, 150, 150, 0.87)";   
})

buttons.forEach((btn) =>{
    btn.addEventListener("click", () =>{
        if(!btn.id.match('erase')){
            screenValue.push(btn.value)
            currentInput.innerHTML = screenValue.join('');

            if(btn.classList.contains('num_btn')){
                answerScreen.innerHTML = eval(screenValue.join(''));
            }
        }
        if(btn.id.match('erase')){
            screenValue.pop()
            currentInput.innerHTML = screenValue.join('');
            answerScreen.innerHTML = eval(screenValue.join(''));
        }
        if(btn.id.match('evaluate')) {
            currentInput.className = 'answerScreen';
            answerScreen.className = 'currentInput';
            answerScreen.style.color = "white";
        }
        if(typeof eval(screenValue.join('')) =='undefined') {
            answerScreen.innerHTML = 0;
        }
    })
})







































